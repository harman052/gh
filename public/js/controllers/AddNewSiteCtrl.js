angular.module('AddNewSiteCtrl', []).controller('AddNewSiteController', function($scope, addNewSite, $http, common, afterSuccess, onError) {

    var vm = this;

    vm.site = {};
    
    var allStatus = common.getData('/workers/site-status').query();

    vm.siteFields = [
        {
            key: 'site_name',
            type: 'input',
            templateOptions: {
                type: 'text',
                label: 'Site Name'
            }
        },
        {
            key: 'site_location',
            type: 'input',
            templateOptions: {
                type: 'text',
                label: 'Site Location',
            }
        },
        {
            key: 'start_date',
            type: 'input',
            templateOptions: {
                type: 'date',
                label: "Start Date" 
            }
        },
        {
            key: 'close_date',
            type: 'input',
            templateOptions: {
                type: 'date',
                label: "Close Date",
            }
        },
        {
            key: 'status_id',
            type: 'select',
            templateOptions: {
                type: 'select',
                label: 'Status',
                labelProp: 'status',
                valueProp: 'status_id',
                options: allStatus,

            }
        }
    ];

    

    $scope.SendData = function(vm) {
        postData = {
            site_name: vm.site_name, site_location: vm.site_location,
            start_date: vm.start_date, 
            close_date: vm.close_date,
            status_id: vm.status_id
        };

        console.log("sd: "+vm.start_date);
        console.log("cd: "+vm.close_date);

        $scope.closeDate = vm.close_date;         
        console.log("closeDate: " + $scope.closeDate);

        addNewSite.postAddNewSite(postData).then(function(response){
            var message = 'Site successfully added.';
            var url = '#/sites';
            afterSuccess.showToast(message);
            afterSuccess.redirect(url);
        }, function(response){
            $scope.error = response.data;
            $scope.errorHeading = 'Following error occurred while adding new site: ';
            onError.scrollUp(response.data);
        });
    }
});
