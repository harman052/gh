angular.module('AddNewStatusCtrl', []).controller('AddNewStatusController', function($scope, $http, common, afterSuccess, onError) {

    var vm = this;

    vm.status = {};

    vm.statusFields = [
        {
            key: 'status',
            type: 'input',
            templateOptions: {
                type: 'text',
                label: 'status Name'
            }
        },
    ];

    $scope.SendData = function(vm) {
        postData = {
            status: vm.status
        };
        var path = '/workers/site-status/add-new';
        common.sendData(postData, path).then(function(response){
            var message = 'status successfully added.';
            var url = '#/site-status';
            afterSuccess.showToast(message);
            afterSuccess.redirect(url);
        }, function(response){
            $scope.error = response.data;
            $scope.errorHeading = 'Following error occurred while adding new status: ';
            onError.scrollUp(response.data);
        });
    }
});
