angular.module('WorkerRatesCtrl', []).controller('WorkerRatesController', function($scope, sendValue, dropDownDialog, $timeout, Excel, pagination) {

    /* Pagination */
    $scope.query = pagination.query('workerName', 25, 1);
    $scope.options = pagination.options(true, true, true);
    $scope.limitOptions = pagination.limitOptions($scope.allSites, 25);


    /* Generating array of objects for year dropdown. This will 
     * always generate past 10 years from the current year.
     */
    var date = new Date();
    var year = date.getFullYear();
    var start = year-10;
    $scope.years = [];
    for (var i = 0; i < 10; i++){
        $scope.years[i] = {id: start + i + 1, value: start + i + 1};
    }

    /* Model */
    $scope.selectedYear = '';

    /* Initiating variable with current year */
    $scope.showYear = year;

    /* Getting rates of workers for current year. By default, rates of 
     * current months will be shown. Rates for past years can be
     * accessed by selecting year from dropdown which appears when
     * "select year" is clicked. We are passing object containing
     * value of current year to this service.
     */
    var routeURL = '/workers/rates';
    sendValue.sendYearValue({year: year}, routeURL)
        .then(function(response){
        $scope.rates = response.data;
    }, function(response){
        $scope.error = response.data;
    });


    /* This function runs when button "select year" is clicked. It
     * shows the dialog of year dropdown with the help of dropDownDialog
     * service. After user selects the year, the response received is 
     * passed to another service "workerOldRates" that calls its
     * function to send object (that contains year selected by user)
     * to backend. Backend respond with rates of workers for all months
     * for selected year.
     */
    $scope.yearSelection = function(){
        var templatePath = '../views/yearSelect.tmpl.html';
        dropDownDialog.showDialog(templatePath).then(function(response){
            $scope.showYear = response;
            sendValue.sendYearValue({year: response}, routeURL)
            .then(function(response){
                $scope.rates = response.data;
            }, function(response){
                $scope.error = response.data;
            });
        }, function(response){
            $scope.error = response;
        });        
    }

    /* Download current view of table data */
    $scope.exportToExcel = function(tableId){
        $scope.exportHref = Excel.tableToExcel(tableId,'sheet name');
        $timeout(function(){
            location.href = $scope.exportHref;
        },100);
    }

});
