angular.module('EditSiteCtrl', []).controller('EditSiteController', function($scope, sitesData, getEditSiteData,postEditSiteData, $http, $httpParamSerializerJQLike, $routeParams, common, afterSuccess, onError) {
    
    /* Fetching sites data */
    var sd = sitesData.query();

    var allStatus = common.getData('/workers/site-status').query();

    /* Creating an array to be used for getting site record 
     * values from database 
     */
    var siteData = [];

    var vm = this;

    /* Using "getEditSiteData" service to get all details of site
     * with the help of site_id. This site_id is get from URL
     * using $routParams inbuilt service. getSiteData() is a function
     * in getEditSiteData service that communicates with REST API to 
     * get site's data.
     */
    getEditSiteData.getSiteData($routeParams.site_id)
    .then(function(response){
        siteData = response.data;
        var starting_date = new Date(siteData[0].start_date);
        var closing_date = new Date(siteData[0].close_date);

        console.log("closeing_date: "+closing_date);
        console.log("from db" + siteData[0].close_date);

        if(siteData[0].start_date == '0000-00-00'){
            starting_date = null;            
        }

        if(siteData[0].close_date == '0000-00-00'){
            closing_date = null;            
        }

        /* Passing values to model to show up in form fields */
        vm.site = {
            site_name: siteData[0].site_name,
            site_location: siteData[0].site_location,
            start_date: starting_date,
            close_date: closing_date,
            status_id: siteData[0].status_id
        };
    }, function(response){
        $scope.editSDError = response.data;
    });

    vm.site = {};

    /* Declaring form elements */
    vm.siteFields = [
        {
            key: 'site_name',
            type: 'input',
            templateOptions: {
                type: 'text',
                label: 'Site Name',
                required: true
            }
        },
        {
            key: 'site_location',
            type: 'input',
            templateOptions: {
                type: 'text',
                label: 'Site Location',
            }
        },
        {
            key: 'start_date',
            type: 'input',
            templateOptions: {
                type: 'date',
                label: "Starting Date",
            }
        },
        {
            key: 'close_date',
            type: 'input',
            templateOptions: {
                type: 'date',
                label: "Closing Date",
            }
        },
        {
            key: 'status_id',
            type: 'select',
            templateOptions: {
                type: 'select',
                label: 'Status',
                labelProp: 'status',
                valueProp: 'status_id',
                options: allStatus,

            }
        }   
    ];

    /* Defining function to be run upon form submission  */
    $scope.SendData = function(vm) {
        postData = {
            site_name: vm.site_name, site_location: vm.site_location,
            start_date: vm.start_date, close_date: vm.close_date,
            status_id: vm.status_id
        };

        console.log("name: "+vm.site_name);
        console.log("sd: "+vm.start_date);
        console.log("cd: "+vm.close_date);

        /* Calling  postUserData function of postEditSiteData service 
         * to send data to database
         */
        postEditSiteData.postUserData($routeParams.site_id, postData)
        .then(function(response){
            var message = 'Site details successfully updated.';
            var url = '#/sites';
            afterSuccess.showToast(message);
            afterSuccess.redirect(url);
        }, function(response){
            $scope.error = response.data;
            $scope.errorHeading = 'Following error occurred while updating site details: ';
            onError.scrollUp(response.data);
        });
    }
});
