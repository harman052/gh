angular.module('SitesCtrl', []).controller('SitesController', function($scope, sitesData, remove, $route, $timeout, Excel, $mdDialog, pagination) {
    $scope.allSites = sitesData.query();

    $scope.selected = [];

    /* Pagination */
    $scope.query = pagination.query('site_name', 25, 1);
    $scope.options = pagination.options(true, true, true);
    $scope.limitOptions = pagination.limitOptions($scope.allSites, 25);

    $scope.deleteSites = function(ev){
        var confirm = $mdDialog.confirm()
            .title('Are you sure to delete the site?')
            .textContent('This action will permanently delete all details of this site from the system.')
            .ariaLabel('Lucky day')
            .targetEvent(ev)
            .ok('Delete')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function() {
            var i; 
            var siteIdArray = [];
            for (i = 0; i < $scope.selected.length; i++){
                siteIdArray[i] = $scope.selected[i].site_id;
                console.log("id in fn array: " + siteIdArray[i]);
            }
            siteIdObject = {siteIds: siteIdArray};
            console.log(siteIdObject.siteIds);
            path = '/sites/delete';
            remove.item(siteIdObject, path).then(function(response){
                $scope.success = response.data;
                $route.reload();
            }, function(response){
                $scope.error = response.data;
            });
        }, function() {
            $scope.status = "Worker deletion cancelled.";
        });
    }

    /* Download current view of table data */
    $scope.exportToExcel=function(tableId){
        $scope.exportHref=Excel.tableToExcel(tableId,'sheet name');
        $timeout(function(){location.href=$scope.exportHref;},100);
    }

});
