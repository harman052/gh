angular.module('StatusCtrl', []).controller('StatusController', function($scope, $route, $timeout, Excel, $mdDialog, common, remove, dropDownDialog, afterSuccess, onError, pagination) {

    $scope.allStatus = common.getData('/workers/site-status').query();

    $scope.selected = [];

    /* Pagination */
    $scope.query = pagination.query('status', 25, 1);
    $scope.options = pagination.options(true, true, true);
    $scope.limitOptions = pagination.limitOptions($scope.allSites, 25);

    $scope.deleteStatus = function(ev){
        var confirm = $mdDialog.confirm()
            .title('Are you sure to delete the status?')
            .textContent('This action will permanently delete all details of this status from the system.')
            .ariaLabel('Delete status confirmation')
            .targetEvent(ev)
            .ok('Delete')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function() {
            var i; 
            var statusIdArray = [];
            for (i = 0; i < $scope.selected.length; i++){
                    statusIdArray[i] = $scope.selected[i].status_id;
                    console.log("id in fn array: " + statusIdArray[i]);
            }
            statusIdObject = {statusIds: statusIdArray};
            path = '/workers/site-status/delete';
            console.log(statusIdObject.statusIds);
            remove.item(statusIdObject, path).then(function(response){
                $scope.success = response.data;
                $route.reload();
            }, function(response){
                $scope.error = response.data;
            });
        }, function() {
            $scope.status = "status deletion cancelled.";
        });
    }

    $scope.editStatus = function(status, status_id, ev){
        var templatePath = '../views/editStatus.tmpl.html';
        dropDownDialog.singleInputDialog(templatePath, status, status_id, ev).then(function(answer){
            statusData = { 
                status: answer.item,
                status_id: answer.item_id
            }
            console.log("status: " + answer.item);
        var path = '/workers/site-status/edit';
        common.sendData(statusData, path).then(function(response){
            var message = 'status successfully updated.';
            $route.reload();
        }, function(response){
            $scope.error = response.data;
            $scope.errorHeading = 'Following error occurred while updating new status: ';
            onError.scrollUp(response.data);
        });
        }, function(){
            
        });
    }

    /* Download current view of table data */
    $scope.exportToExcel=function(tableId){
        $scope.exportHref=Excel.tableToExcel(tableId,'sheet name');
        $timeout(function(){location.href=$scope.exportHref;},100);
    }

});
