angular.module('EditWorkerCtrl', []).controller('EditWorkerController', function($scope, workersData, getEditWorkerData,postEditWorkerData, sitesData, $http, $httpParamSerializerJQLike, $routeParams, common, $window, $mdToast, $document, $timeout, afterSuccess, onError) {

    var genderArray = [{"name": "Male","id": "M"},
      {"name": "Female","id": "F"}]
    
    /* Fetching sites data */
    var sd = sitesData.query();

    /* Fetching worker designations */
    var designations = common.getData('/workers/posts').query();

    /* Creating an array to be used for getting worker record 
     * values from database 
     */
    var userData = [];

    var vm = this;

    /* Using "getEditWorkerData" service to get all details of worker
     * with the help of worker_id. This worker_id is get from URL
     * using $routeParams inbuilt service. getWorkerData() is a function
     * in getEditWorkerData service that communicates with REST API to get
     * worker's data.
     */
    getEditWorkerData.getWorkerData($routeParams.worker_id).then(function(response){
        userData = response.data;
        var dob = new Date(userData[0].date_of_birth);
        var doj = new Date(userData[0].joining_date);

        if(userData[0].date_of_birth == '0000-00-00'){
            dob = null;            
        }

        if(userData[0].joining_date == '0000-00-00'){
            doj = null;            
        }


        /* Passing values to model to show up in form fields */
        vm.worker = {
            first_name:userData[0].first_name,
            father_first_name:userData[0].father_first_name,
            sex: userData[0].sex,
            address: userData[0].address,
            phone: userData[0].phone,
            date_of_birth: dob,
            post_id: userData[0].post_id,
            joining_date: doj,
        };
    }, function(response){
        $scope.editWDError = response.data;
    });

    vm.worker = {};

    /* Declaring form elements */
    vm.workerFields = [
        {
            key: 'first_name',
            type: 'input',
            templateOptions: {
                type: 'text',
                label: 'First Name',
                placeholder: 'Enter first name of worker',
                required: true
            }
        },
        {
            key: 'father_first_name',
            type: 'input',
            templateOptions: {
                type: 'text',
                label: "Father's First Name",
                placeholder: "Enter first name of worker's father",
            }
        },
        {
            key: 'sex',
            type: 'select',  
            templateOptions: {
                label: "Select gender",
                labelProp: "name",
                valueProp: "id",
                options: genderArray
            }
        },
        {
            key: 'address',
            type: 'textarea',
            templateOptions: {
                label: "Address",
                placeholder: "Enter address of worker",
                rows: 5,
            }
        },
        {
            key: 'phone',
            type: 'input',
            templateOptions: {
                type: 'number',
                label: "Phone number",
                placeholder: "Enter phone number of worker",
            }
        },
        {
            key: 'date_of_birth',
            type: 'input',
            templateOptions: {
                type: 'date',
                label: 'Date of birth',
                placeholder: "Enter date of birth",
            }
        },
        {
            key: 'post_id',
            type: 'select',
            templateOptions: {
                label: "Post",
                labelProp: "post",
                valueProp: "post_id",
                options: designations
            }
        },
        {
            key: 'joining_date',
            type: 'input',
            templateOptions: {
                type: 'date',
                label: "Date of joining",
                placeholder: "Enter date of joining",
            }
        },
    ];

    /* Defining function to be run upon form submission  */
    $scope.SendData = function(vm) {
        postData = {
            first_name: vm.first_name,
            father_first_name: vm.father_first_name, 
            sex: vm.sex, address: vm.address, phone: vm.phone,
            date_of_birth: vm.date_of_birth, post_id: vm.post_id, 
            joining_date: vm.joining_date
        };

        /* Calling  postUserData function of postEditWorkerData service 
         * to send data to database
         */
        postEditWorkerData.postUserData($routeParams.worker_id, postData).then(function(response){
            var message = 'Worker details successfully updated.';
            var url = '#/workers';
            afterSuccess.showToast(message);
            afterSuccess.redirect(url);
        }, function(response){
            $scope.error = response.data;
            $scope.errorHeading = 'Following error occurred while updating worker details: ';
            onError.scrollUp(response.data);
        });
    }
});
