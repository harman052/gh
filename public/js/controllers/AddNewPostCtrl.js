angular.module('AddNewPostCtrl', []).controller('AddNewPostController', function($scope, $http, common, afterSuccess, onError) {

    var vm = this;

    vm.post = {};

    vm.postFields = [
        {
            key: 'post',
            type: 'input',
            templateOptions: {
                type: 'text',
                label: 'Post Name'
            }
        },
    ];

    $scope.SendData = function(vm) {
        postData = {
            post: vm.post
        };
        var path = '/workers/posts/add-new';
        common.sendData(postData, path).then(function(response){
            var message = 'Post successfully added.';
            var url = '#/posts';
            afterSuccess.showToast(message);
            afterSuccess.redirect(url);
        }, function(response){
            $scope.error = response.data;
            $scope.errorHeading = 'Following error occurred while adding new post: ';
            onError.scrollUp(response.data);
        });
    }
});
