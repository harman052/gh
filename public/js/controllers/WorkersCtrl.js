angular.module('WorkersCtrl', []).controller('WorkersController', function($scope, workersData, sitesData, remove, $route, $mdDialog, $q, $timeout, addWorkersToSite, workerAndSiteId, dropDownDialog, Excel, pagination) {

    $scope.allWorkers = workersData.query();
    $scope.allSites = sitesData.query();

    /* Regular expression to accept only upto 3 decimal places */
    $scope.regex = '^[0-9]+(\.[0-9]{1,3})?$';

    $scope.selected = [];
    $scope.selectedSite = '';

    /* Pagination */
    $scope.query = pagination.query('first_name', 25, 1);
    $scope.options = pagination.options(true, true, true);
    $scope.limitOptions = pagination.limitOptions($scope.allWorkers, 25);

    /* Delete workers selected by user. */
    $scope.deleteWorkers = function(ev) {
    var path = '/workers/delete';
    /* Appending dialog to document.body to cover sidenav in docs app */
    var confirm = $mdDialog.confirm()
          .title('Are you sure to delete the worker?')
          .textContent('This action will permanently delete worker and his/her salary details from the system.')
          .ariaLabel('Lucky day')
          .targetEvent(ev)
          .ok('Delete')
          .cancel('Cancel');
    $mdDialog.show(confirm).then(function() {
        remove.item(workerAndSiteId.getWorkerAndSiteId($scope.selected), path).then(function(response){
           $scope.success = response.data;
           $route.reload();
        }, function(response){
            $scope.error = response.data;
        });
    }, function() {
      $scope.status = "Worker deletion cancelled.";
    });
  }


    /* Calling dialog service to show dropdown of sites in a dialog */
    $scope.assignWorkersToSite = function(ev) {
        var templatePath = '../views/siteDropDownDialog.tmpl.html';
        var urlPath = '/workers/move-to-site';
        dropDownDialog.showDialog(templatePath, ev).then(function(answer) {
            $scope.status = 'You said the information was "' + answer + '".';
            addWorkersToSite.moveWorkersToSite(workerAndSiteId.getWorkerAndSiteId($scope.selected, answer), urlPath)
            .then(function(response){
                $scope.success = response.data;
                $route.reload();
            }, function(response){
                $scope.error = response.data;
            });
        }, function() {
                $scope.status1 = 'You cancelled the dialog.';
        });    
    };

    /* Calling dialog service to show rate input field in a dialog */
    $scope.setRates = function(ev) {
        var templatePath = '../views/rateDialog.tmpl.html';
        var urlPath = '/workers/set-rate';
        dropDownDialog.showDialog(templatePath, ev).then(function(answer) {
            $scope.status = 'You said the information was "' + answer + '".';
            addWorkersToSite.moveWorkersToSite(workerAndSiteId.getWorkerAndSiteId($scope.selected, answer), urlPath)
            .then(function(response){ 
                $scope.success = response.data;
                if($scope.success == 0){
                    $mdDialog.show(
                        $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Rate cannot be set')
                        .textContent('Rate of worker can be set only after he/she added to site.')
                        .ariaLabel('Worker not assigned to any site')
                        .targetEvent(ev)
                        .ok('OK')
                        );
                } else {
                     $route.reload();
                }
            }, function(response){
                $scope.error = response.data;
            });
        }, function() {
                $scope.status1 = 'You cancelled the dialog.';
        });    
    };


    /* Download current view of table data */
    $scope.exportToExcel=function(tableId){
        $scope.exportHref=Excel.tableToExcel(tableId,'sheet name');
        $timeout(function(){location.href=$scope.exportHref;},100);
    }

});
