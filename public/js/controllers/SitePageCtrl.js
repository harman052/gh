angular.module('SitePageCtrl', []).controller('SitePageController', function($scope, getWorkersOnSite, $routeParams, getEditSiteData, $mdDialog, $mdMedia, addWorkersToSite, workerAndSiteId, $route, dropDownDialog, salaryDialog, salaryDetails, $route, $timeout, Excel, getSalaryDetails, sendValue, monthYearDialog, remove) {
    
    $scope.regex = '^[0-9]+(\.[0-9]{1,3})?$';
    $scope.selected = [];
    $scope.workers = [];
    $scope.selection = {};
    $scope.siteID = $routeParams.site_id;
    $scope.query = {
        order: 'first_name',
        limit: 10,
        page: 1
    };

    $scope.options = {
        boundaryLinks: true,
        limitSelect: true,
        pageSelect: true
    };

    $scope.limitOptions = [10, 20, {
        label: 'All',
        value: function () {
            return $scope.allWorkers ? $scope.allWorkers.length : 0;
        }
    }];

    $scope.onPaginate = function(page, limit) {
        console.log('Scope Page: ' + $scope.query.page + ' Scope Limit: ' + $scope.query.limit);
        console.log('Page: ' + page + ' Limit: ' + limit);
        $scope.promise = $timeout(function () {}, 2000);
    };

    $scope.logPagination = function (page, limit) {
    console.log('page: ', page);
    console.log('limit: ', limit);
  }

    /* mdOnSelect directive pass selected items as arguments 
     * to function. 
     */
    $scope.selectedItems = function (item) { 
            console.log(item.worker_id, 'was selected');
              };

    getEditSiteData.getSiteData($routeParams.site_id).then(function(response){
        $scope.siteData = response.data;
        $scope.siteName = $scope.siteData.site_name;
    }, function(response){
        $scope.editSDError = response.data;
    });

                function amtBalTotal(workerData){
                var totalAmount = 0;
                var totalBalance = 0;
                for(var i in workerData){
                    totalAmount += workerData[i].amount; 
                    totalBalance += workerData[i].balance; 
                }
                return $scope.sum = {amount: totalAmount, balance: totalBalance}
               }


    getWorkersOnSite.getWorkers($routeParams.site_id).then(function(response){
        
        $scope.workers = response.data;
    
        /* Initiating variables with current year and month */
        $scope.showYear = $scope.workers[0].year;
        $scope.showMonth = monthValue[$scope.workers[0].month-1];
       
/*        $scope.totalAmount = 0;
        $scope.totalBalance = 0;
 
        for(var i in $scope.workers){
            $scope.totalAmount += $scope.workers[i].amount; 
            $scope.totalBalance += $scope.workers[i].balance; 
        }*/
        amtBalTotal($scope.workers);

    }, function(response){
        $scope.editSDError = response.data;
    });

    var dropDownTemplate = '../views/siteDropDownDialog.tmpl.html';

    /* Site dropown dialog */
    $scope.assignWorkersToSite = function(ev) {
        var urlPath = '/workers/move-to-site';
        dropDownDialog.showDialog(dropDownTemplate, ev).then(function(answer) {
            $scope.status = 'You said the information was "' + answer + '".';
            addWorkersToSite.moveWorkersToSite(workerAndSiteId.getWorkerAndSiteId($scope.selected, answer), urlPath)
            .then(function(response){
                $scope.success = response.data;
                $route.reload();
            }, function(response){
                $scope.error = response.data;
            });
        }, function() {
                $scope.status1 = 'You cancelled the dialog.';
        });    
    }; 

    var editSalaryTemplate = '../views/editSalary.tmpl.html';
    
    $scope.editSalaryDetails = function(sal_id, workerId, workerName, rate, $event) {
        postDataObject = {
            sal_id: sal_id
        }
        getSalaryDetails.sendData(workerId, postDataObject)
        .then(function(response){
            var result = response.data;
            salaryDialog.showDialog(editSalaryTemplate, 
                workerName, rate, result[0].no_of_days, 
                result[0].advance, $event).then(function(answer) {
                    $scope.days = answer.no_of_days;
                    $scope.adv = answer.advance;
                    postData = {
                                no_of_days: answer.no_of_days,
                                advance: answer.advance,
                                worker_id: workerId,
                                sal_id: sal_id
                    }
                    salaryDetails.sendSalaryDetails(postData)
                    .then(function(response){
                        $scope.success = response.data;    
                        $route.reload();
                    }, function(response){
                        $scope.error = response.data;
                    });
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
        }, function(response){
            $scope.error = response.data;
        });
    }


    $scope.createSheet = function(){
        var urlPath = '/sites/new-month-sheet/:site_id';

        /* Getting all workers who are currently assigned to this site */
        getWorkersOnSite.getWorkers($routeParams.site_id).then(function(response){
            $scope.workers = response.data;

            /* Passing workers array and site Id so that we can get
             * latest records of workers from salary table. These 
             * latest records will help us filtering the workers that
             * had worked at this site this month but have been 
             * moved to other sites. Only workers whose latest record
             * shows site_id of this site will be added to that site 
             * for new month.
             */
            addWorkersToSite.moveWorkersToSite(workerAndSiteId.getWorkerAndSiteId($scope.workers, $routeParams.site_id), urlPath)
            .then(function(response){
                $scope.success = response.data;
/*                $route.reload();*/
                $scope.workerArray = [];

                /* for loop to iterate over the records received */
                for (var i = 0; i < $scope.success.length; i++){

                    /* if condition to filter out moved workers */
                    if($scope.success[i].site_id == $routeParams.site_id){
                        $scope.workerArray[i] = $scope.success[i];
                    } else {
                        $scope.notForThisSite = $scope.success[i];
                    }
                    console.log("workerArray: "+$scope.workerArray[i]);
                }

                /* Sending filtered workers array with site id to add
                 * them to this site for new month 
                 */
                    console.log("workerArray at 0: "+$scope.workerArray[0]);
                var addWorkerPath = '/sites/new-month/add-workers';
        addWorkersToSite.moveWorkersToSite(workerAndSiteId.getWorkerAndSiteId($scope.workerArray, $routeParams.site_id), addWorkerPath).then(function(response){
                    $scope.workersAdded = response.data;
                    $route.reload();
                }, function(response){
                    $scope.error = response.data;
                    console.log("Error adding workers to this site for new month.");
                });

            }, function(response){
                $scope.error = response.data;
                console.log("Failed to fetch latest records of current workers.");
            });
        }, function (response) {
            $scope.error = response.data;
            console.log("workers data not received");
        });
    }

    /* Generating array of objects for year dropdown */
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth();
    var start = year-10;
    $scope.years = [];
    for (var i = 0; i < 10; i++){
        $scope.years[i] = {id: start + i + 1, value: start + i + 1};
    }

    $scope.months = [];
    var monthValue = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    for (var i = 0; i < 12; i++){
        $scope.months[i] = {id: i + 1, value: monthValue[i]};
    }


    $scope.yearSelection = function(){
        var templatePath = '../views/yearAndMonthSelect.tmpl.html';
        var routeURL = '/sites/oldData';
        monthYearDialog.showDialog(templatePath).then(function(response){
            $scope.postData = {
                        year: response.year,
                        month: response.month,
                        site_id: $routeParams.site_id
            }
            $scope.showobj = response;
            $scope.showYear = response.year;
            $scope.showMonth = monthValue[response.month - 1];
            sendValue.sendYearValue($scope.postData, routeURL).then(function(response){
                $scope.workers = response.data;
                amtBalTotal($scope.workers);
            }, function(response){
                $scope.error = response.data;
            });
        }, function(response){
            $scope.error = response;
        });        
    }

    /* Delete workers selected by user. */
    $scope.deleteWorkers = function(sal_id, ev) {
    var path = '/sites/delete-workers';;
    /* Appending dialog to document.body to cover sidenav in docs app */
    var confirm = $mdDialog.confirm()
          .title('Are you sure to delete the worker?')
          .textContent('This action will permanently delete worker and his/her salary details from the system.')
          .ariaLabel('Lucky day')
          .targetEvent(ev)
          .ok('Delete')
          .cancel('Cancel');
    $mdDialog.show(confirm).then(function() {
        remove.item(workerAndSiteId.getSalId($scope.selected), path).then(function(response){
           $scope.success = response.data;
           $route.reload();
        }, function(response){
            $scope.error = response.data;
        });
    }, function() {
      $scope.status = "Worker deletion cancelled.";
    });
  }


    /* Download current view of table data */
    $scope.exportToExcel=function(tableId){
        $scope.exportHref=Excel.tableToExcel(tableId,'sheet name');
        $timeout(function(){location.href=$scope.exportHref;},100);
    }

});
