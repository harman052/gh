angular.module('PostsCtrl', []).controller('PostsController', function($scope, $route, $timeout, Excel, $mdDialog, common, remove, dropDownDialog, afterSuccess, onError, pagination) {
    $scope.allPosts = common.getData('/workers/posts').query();

    $scope.selected = [];

    /* Pagination */
    $scope.query = pagination.query('post', 25, 1);
    $scope.options = pagination.options(true, true, true);
    $scope.limitOptions = pagination.limitOptions($scope.allSites, 25);


    $scope.deletePosts = function(ev){
        var confirm = $mdDialog.confirm()
            .title('Are you sure to delete the post?')
            .textContent('This action will permanently delete all details of this post from the system.')
            .ariaLabel('Delete post confirmation')
            .targetEvent(ev)
            .ok('Delete')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function() {
            var i; 
            var postIdArray = [];
            for (i = 0; i < $scope.selected.length; i++){
                    postIdArray[i] = $scope.selected[i].post_id;
                    console.log("id in fn array: " + postIdArray[i]);
            }
            postIdObject = {postIds: postIdArray};
            path = '/workers/posts/delete';
            console.log(postIdObject.postIds);
            remove.item(postIdObject, path).then(function(response){
                $scope.success = response.data;
                $route.reload();
            }, function(response){
                $scope.error = response.data;
            });
        }, function() {
            $scope.status = "post deletion cancelled.";
        });
    }

    $scope.editPost = function(post, post_id, ev){
        var templatePath = '../views/editPost.tmpl.html';
        dropDownDialog.singleInputDialog(templatePath, post, post_id, ev).then(function(answer){
            postData = { 
                post: answer.item,
                post_id: answer.item_id
            }
        var path = '/workers/posts/edit';
        common.sendData(postData, path).then(function(response){
            var message = 'Post successfully updated.';
            $route.reload();
        }, function(response){
            $scope.error = response.data;
            $scope.errorHeading = 'Following error occurred while updating new post: ';
            onError.scrollUp(response.data);
        });
        }, function(){
            
        });
    }

    /* Download current view of table data */
    $scope.exportToExcel=function(tableId){
        $scope.exportHref=Excel.tableToExcel(tableId,'sheet name');
        $timeout(function(){location.href=$scope.exportHref;},100);
    }

});
