angular.module('ReportsCtrl', []).controller('ReportsController', function($scope, sendValue, dropDownDialog, Excel, $timeout, pagination) {

    /* Pagination */
    $scope.query = pagination.query('siteName', 25, 1);
    $scope.options = pagination.options(true, true, true);
    $scope.limitOptions = pagination.limitOptions($scope.allSites, 25);


    /* Generating array of objects for year dropdown */
    var date = new Date();
    var year = date.getFullYear();
    var start = year-10;
    $scope.years = [];
    for (var i = 0; i < 10; i++){
        $scope.years[i] = {id: start + i + 1, value: start + i + 1};
    }

    /* Model */
    $scope.selectedYear = '';

    /* Initiating variable with current year */
    $scope.showYear = year;

        function monthsTotalObj(reports) {
        $scope.totalJan = 0;
        $scope.totalFeb = 0;
        $scope.totalMar = 0;
        $scope.totalApr = 0;
        $scope.totalMay = 0;
        $scope.totalJun = 0;
        $scope.totalJul = 0;
        $scope.totalAug = 0;
        $scope.totalSep = 0;
        $scope.totalOct = 0;
        $scope.totalNov = 0;
        $scope.totalDecm = 0;

        for(var i in reports){

            /* Summing up the columns i.e. month wise total of expenses */
            $scope.totalJan += reports[i].Jan;
            $scope.totalFeb += reports[i].Feb;
            $scope.totalMar += reports[i].Mar;
            $scope.totalApr += reports[i].Apr;
            $scope.totalMay += reports[i].May;
            $scope.totalJun += reports[i].Jun;
            $scope.totalJul += reports[i].Jul;
            $scope.totalAug += reports[i].Aug;
            $scope.totalSep += reports[i].Sep;
            $scope.totalOct += reports[i].Oct;
            $scope.totalNov += reports[i].Nov;
            $scope.totalDecm += reports[i].Decm;
        }
    }

 
    var routeURL = '/sites/reports';
    sendValue.sendYearValue({year: year}, routeURL).then(function(response){
        $scope.reports = response.data;
        monthsTotalObj($scope.reports);

            /* Summing up rows i.e. site wise total of expenses. 
             * Since we are not getting row wise horizontal total 
             * from database so here we calculating it through "for 
             * loop" that iterates over the $scope.reports array of 
             * objects. Each object from this array holds expenses of 
             * all months for particular site. By iterating through 
             * this array, we are adding "Total" property to its 
             * element objects. The "Total" property holds the sum 
             * of all months for each specific site. 
             */
        for(var i in $scope.reports){
            $scope.reports[i]['Total'] = $scope.reports[i].Jan +  
                                        $scope.reports[i].Feb + 
                                        $scope.reports[i].Mar + 
                                        $scope.reports[i].Apr + 
                                        $scope.reports[i].May + 
                                        $scope.reports[i].Jun + 
                                        $scope.reports[i].Jul + 
                                        $scope.reports[i].Aug + 
                                        $scope.reports[i].Sep + 
                                        $scope.reports[i].Oct + 
                                        $scope.reports[i].Nov + 
                                        $scope.reports[i].Decm;
        }
    }, function(response){
        $scope.error = response.data;
    });


    /* This function runs when button "select year" is clicked. It
     * shows the dialog of year dropdown with the help of dropDownDialog
     * service. After user selects the year, the response received is 
     * passed to another service "workerOldRates" that calls its
     * function to send object (that contains year selected by user)
     * to backend. Backend respond with rates of workers for all months
     * for selected year.
     */
    $scope.yearSelection = function(){
        var templatePath = '../views/yearSelect.tmpl.html';
    dropDownDialog.showDialog(templatePath).then(function(response){
        $scope.showYear = response;
         sendValue.sendYearValue({year: response}, routeURL).then(function(response){
            $scope.reports = response.data;
            monthsTotalObj($scope.reports);

            /* Summing up rows i.e. site wise total of expenses. 
             * Since we are not getting row wise horizontal total 
             * from database so here we calculating it through "for 
             * loop" that iterates over the $scope.reports array of 
             * objects. Each object from this array holds expenses of 
             * all months for particular site. By iterating through 
             * this array, we are adding "Total" property to its 
             * element objects. The "Total" property holds the sum 
             * of all months for each specific site. 
             */
        for(var i in $scope.reports){

            $scope.reports[i]['Total'] = $scope.reports[i].Jan +  
                                        $scope.reports[i].Feb + 
                                        $scope.reports[i].Mar + 
                                        $scope.reports[i].Apr + 
                                        $scope.reports[i].May + 
                                        $scope.reports[i].Jun + 
                                        $scope.reports[i].Jul + 
                                        $scope.reports[i].Aug + 
                                        $scope.reports[i].Sep + 
                                        $scope.reports[i].Oct + 
                                        $scope.reports[i].Nov + 
                                        $scope.reports[i].Decm;
        }
        }, function(response){
            $scope.error = response.data;
        });
    }, function(response){
            $scope.error = response;
        });        
    }

$scope.exportToExcel=function(ta){ // ex: '#my-table'
    $scope.exportHref=Excel.tableToExcel(ta,'sheet name');
    $timeout(function(){location.href=$scope.exportHref;},100); // trigger download
}

});
