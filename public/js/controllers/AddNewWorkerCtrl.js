angular.module('AddNewWorkerCtrl', []).controller('AddNewWorkerController', function($scope, workersData, sitesData, addNewWorker, $http, common, afterSuccess, onError) {

    $scope.wd = workersData.query();
   var designations = common.getData('/workers/posts').query();

    var genderArray = [{"name": "Male","id": "M"},
      {"name": "Female","id": "F"}]
    
    var sd = sitesData.query();

    var vm = this;

    vm.worker = {};

    vm.workerFields = [
        {
            key: 'first_name',
            type: 'input',
            templateOptions: {
                type: 'text',
                label: 'First Name'
            }
        },
        {
            key: 'father_first_name',
            type: 'input',
            templateOptions: {
                type: 'text',
                label: "Father's First Name",
            }
        },
        {
            key: 'sex',
            type: 'select',  
            templateOptions: {
                label: "Select gender",
                labelProp: "name",
                valueProp: "id",
                options: genderArray
            }
        },
        {
            key: 'address',
            type: 'textarea',
            templateOptions: {
                label: "Address",
                rows: 5,
            }
        },
        {
            key: 'phone',
            type: 'input',
            templateOptions: {
                type: 'number',
                label: "Phone number",
            }
        },
        {
            key: 'date_of_birth',
            type: 'input',
            templateOptions: {
                type: 'date',
                label: 'Date of birth',
            }
        },
        {
            key: 'post_id',
            type: 'select',
            templateOptions: {
                type: 'select',
                label: "Post",
                labelProp: "post",
                valueProp: "post_id",
                options: designations,
            }
        },
        {
            key: 'joining_date',
            type: 'input',
            templateOptions: {
                type: 'date',
                label: "Date of joining",
            }
        },
    ];

     $scope.SendData = function(vm) {
         postData = {
             first_name: vm.first_name,
             father_first_name: vm.father_first_name, 
             sex: vm.sex, address: vm.address, phone: vm.phone,
             date_of_birth: vm.date_of_birth, post_id: vm.post_id, 
             joining_date: vm.joining_date
        };

         console.log("from controller "+postData.first_name);
        addNewWorker.postAddNewWorker(postData).then(function(response){
            console.log("response received: "+ response.data);
            if(response.data == "1"){
                var message = 'Worker successfully added.';
                var url = '#/workers';
                $scope.er = response.data;
                afterSuccess.showToast(message);
                afterSuccess.redirect(url);
            } else {
                $scope.error = response.data;
                $scope.errorHeading = 'Following error occurred while adding worker: ';
                onError.scrollUp();
            }

        }, function(response){
                $scope.error = response.data;
                $scope.errorHeading = 'Following error occurred while adding worker: ';
                onError.scrollUp();
        });
    }
});
