angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

	$routeProvider

		/* Specifing templates to routes. At root route i.e. '/'
         * home.html will be loaded. Similarly for each other route,
         * a particular template loads. */
		.when('/', {
			templateUrl: 'views/home.html',
			controller: 'MainController'
		})

		.when('/workers', {
			templateUrl: 'views/workers.html',
			controller: 'WorkersController'
		})

		.when('/workers/add-new', {
			templateUrl: 'views/addNewWorker.html',
			controller: 'AddNewWorkerController'
		})

		.when('/workers/edit/:worker_id', {
			templateUrl: 'views/editWorker.html',
			controller: 'EditWorkerController'
		})

		.when('/workers/delete', {
			templateUrl: 'views/workers.html',
			controller: 'WorkersController'
		})

		.when('/sites', {
			templateUrl: 'views/sites.html',
			controller: 'SitesController'	
		})

		.when('/sites/add-new', {
			templateUrl: 'views/addNewSite.html',
			controller: 'AddNewSiteController'	
		})

		.when('/sites/edit/:site_id', {
			templateUrl: 'views/editSite.html',
			controller: 'EditSiteController'
		})	
    
        .when('/sites/:site_id', {
			templateUrl: 'views/sitePage.html',
			controller: 'SitePageController'
		})

        .when('/worker-rates', {
			templateUrl: 'views/workerRates.html',
			controller: 'WorkerRatesController'
		})

        .when('/reports', {
			templateUrl: 'views/reports.html',
			controller: 'ReportsController'
		})

        .when('/posts', {
			templateUrl: 'views/posts.html',
			controller: 'PostsController'
		})


        .when('/posts/add-new', {
			templateUrl: 'views/addNewPost.html',
			controller: 'AddNewPostController'
		})


        .when('/site-status', {
			templateUrl: 'views/status.html',
			controller: 'StatusController'
		})


        .when('/site-status/add-new', {
			templateUrl: 'views/addNewStatus.html',
			controller: 'AddNewStatusController'
		})


        .otherwise({                                                     
        redirectTo: '/'
      });




//	$locationProvider.html5Mode(true);

}]);
