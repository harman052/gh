angular.module('WorkersService', ['ngResource'])


.factory('getEditWorkerData', ['$http', '$httpParamSerializerJQLike', function($http, $httpParamSerializerJQLike) {
    return {
        getWorkerData: function(worker_id){          
            return $http.get("/workers/edit/"+ worker_id +"");
        }
      }
}])


.factory('postEditWorkerData', ['$http', '$httpParamSerializerJQLike', function($http, $httpParamSerializerJQLike) {
    return {
        postUserData: function(worker_id, postData){
        return $http({
                url: "/workers/edit/"+ worker_id +"",
                method: 'POST',
                data: $httpParamSerializerJQLike(postData),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        }
    }
}])


.factory('addNewWorker', ['$http', '$httpParamSerializerJQLike', function($http, $httpParamSerializerJQLike) {
    return {
        postAddNewWorker: function(postData){
            return $http({
                url: '/workers/add-new',
                method: 'POST',
                data: $httpParamSerializerJQLike(postData),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        }
    }
}])


.factory('workersData', ['$resource', function($resource) {
  return $resource('/workers', {}, {
        query: { method:'GET', isArray: true }
               }); 
}])


.factory('common', ['$resource', '$http', '$httpParamSerializerJQLike',  function($resource, $http, $httpParamSerializerJQLike) {
    return {
        getData: function(path){
            return $resource(path, {}, {
                query: { method:'GET', isArray: true }
            }); 
       
        },
        sendData: function(postData, path){
            return $http({
                url: path,
                method: 'POST',
                data: $httpParamSerializerJQLike(postData),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        }
    }
}])


.factory('remove', ['$http', '$httpParamSerializerJQLike', function($http, $httpParamSerializerJQLike) {
    return {
        item: function(IdsObj, path){
            return $http({
                url: path,
                method: 'POST',
                data: $httpParamSerializerJQLike(IdsObj),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        }
    }
}])


.factory('addWorkersToSite', ['$http', '$httpParamSerializerJQLike', function($http, $httpParamSerializerJQLike) {
    return {
        moveWorkersToSite: function(workerIdObj, path){
            return $http({
                url: path,
                method: 'POST',
                data: $httpParamSerializerJQLike(workerIdObj),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        }
    }
}])


/* This service use "arrayOfSelectedWorkers" argument which is an array
 * that gets populated with all fields of rows selected by user from 
 * worker table. So basically, it contains all data of workers selected 
 * by user. We need only worker_id to perform delete operation, so in 
 * order to get it, we are using a "for loop" which iterates through 
 * the "arrayOfSelectedWorkers" and assigns worker_ids to another array 
 * named "workerIdArray". We pass "workerIdArray" into object 
 * "workerIdObject" which is finally returned by this service. 
 *
 * The second argument "siteid" is passed so that this service can also
 * be used to move workers around the sites, i.e. changing their sites. 
 * The "siteid" is also passed into the same object that this service 
 * returns.
 */

.factory('workerAndSiteId', [function() {
    return {
        getWorkerAndSiteId: function(arrayOfSelectedWorkers, siteid){
            var i; 
            var workerIdArray = [];
           
            /* Getting worker_ids of selected workers from "selected" array */
            for (i = 0; i < arrayOfSelectedWorkers.length; i++){
                workerIdArray[i] = arrayOfSelectedWorkers[i].worker_id;
                console.log("workerIdArray: " + workerIdArray[i]);
            }
            console.log("from service: "+siteid);
            return workerIdObject = {workerIds: workerIdArray, siteId: siteid};
        },

        getSalId: function(arrayOfSelectedWorkers){
            var i; 
            var salIdArray = [];
           
            /* Getting worker_ids of selected workers from "selected" array */
            for (i = 0; i < arrayOfSelectedWorkers.length; i++){
                salIdArray[i] = arrayOfSelectedWorkers[i].sal_id;
                console.log("salIdArray: " + salIdArray[i]);
            }
            return salIdObject = {salIds: salIdArray};
        }
    }
}])

/* $mdDialog is an inbuilt service to show up dialog to users. There 
 * are different functions of $mdDialog service are available to be 
 * used in different situations. We have used $mdDialog.show({optionsObject}) 
 * that shows a dialog with the options specified in the optionsObject 
 * arugment. 
 *
 * Dialogs in angular material have isolated scopes. Hence in order 
 * to pass values from parent controller to dialog, we have to pass 
 * them to the dialog controller through "locals" property of 
 * optionsObject argument. "locals" take an object containing key/value 
 * pairs. The keys will be used as names of values to inject into the 
 * controller.
 */
.factory('salaryDialog', ['$mdDialog', function($mdDialog) {
    return {
        showDialog: function(templatePath, workerName, 
        rate, no_of_days, advance, $event){
            return $mdDialog.show({
                controller: function DialogController($scope, 
                $mdDialog, nameOfWorker, rateOfWorker, 
                no_of_days, advance) {
                    $scope.salaryDetails =  {};
                    $scope.workerName = nameOfWorker;
                    $scope.rate = rateOfWorker;
                    $scope.salaryDetails.no_of_days = no_of_days;
                    $scope.salaryDetails.advance = advance;
                    $scope.cancel = function() {
                        $mdDialog.cancel();
                    };
                    $scope.answer = function(answer) {
                        /* Checking if the answer object have values 
                         * for both properties. If any of them is 
                         * missing, dialog will not hide and user 
                         * will not be able to submit data.
                         */
                        if(answer.no_of_days != null && 
                        answer.advance != null){
                            $mdDialog.hide(answer);
                        }
                    };
                },
                templateUrl: templatePath,
                parent: angular.element(document.body),
                targetEvent: $event,
                locals: {
                            nameOfWorker: workerName, 
                            rateOfWorker: rate, 
                            no_of_days: no_of_days, 
                            advance: advance
                        },
                clickOutsideToClose: true
            });
        }
    }
}])

/* Simple dialog to show dropdown of sites */
.factory('dropDownDialog', ['$mdDialog', function($mdDialog) {
    return {
        showDialog: function(templatePath, $event){
            return $mdDialog.show({
                    controller: function DialogController($scope, $mdDialog) { 
                                    $scope.cancel = function() {
                                        $mdDialog.cancel();
                                    };
                                    /* "answer" contains site_id to 
                                     * which workers are assigned and 
                                     * 0 cannot be site id, so if 
                                     * site_id has 0, it means user has
                                     * not selected any site from 
                                     * dropdown, hence, it should not
                                     * be submitted.
                                     */
                                    $scope.answer = function(answer) {
                                        if(answer != 0 && answer != undefined){
                                            $mdDialog.hide(answer);
                                        }
                                    };
                                },
                    templateUrl: templatePath,
                    parent: angular.element(document.body),
                    targetEvent: $event,
                    clickOutsideToClose: true
                });
        },
         singleInputDialog: function(templatePath, item, item_id, $event){
            return $mdDialog.show({
                    controller: function DialogController($scope, $mdDialog, item_id, item) { 
                        $scope.itemDetails = {};
                        $scope.itemDetails.item = item;
                        $scope.itemDetails.item_id = item_id;

                                    $scope.cancel = function() {
                                        $mdDialog.cancel();
                                    };
                                    /* "answer" contains site_id to 
                                     * which workers are assigned and 
                                     * 0 cannot be site id, so if 
                                     * site_id has 0, it means user has
                                     * not selected any site from 
                                     * dropdown, hence, it should not
                                     * be submitted.
                                     */
                                    $scope.answer = function(answer) {
                                        if(answer.item != ''){
                                            $mdDialog.hide(answer);
                                        }
                                    };
                                },
                    templateUrl: templatePath,
                    parent: angular.element(document.body),
                    targetEvent: $event,
                    clickOutsideToClose: true,
                    locals: {
                        item_id: item_id,
                        item: item
                    }
                });
        }   
    }
}])


/* Simple dialog to show dropdown of sites */
.factory('monthYearDialog', ['$mdDialog', function($mdDialog) {
    return {
        showDialog: function(templatePath, $event){
            return $mdDialog.show({
                    controller: function DialogController($scope, $mdDialog) { 
                                    $scope.cancel = function() {
                                        $mdDialog.cancel();
                                    };
                                    /* "answer" contains site_id to 
                                     * which workers are assigned and 
                                     * 0 cannot be site id, so if 
                                     * site_id has 0, it means user has
                                     * not selected any site from 
                                     * dropdown, hence, it should not
                                     * be submitted.
                                     */
                                    $scope.answer = function(answer) {
                                        if(Object.keys(answer).length == 2){
                                            $mdDialog.hide(answer);
                                        }
                                    };
                                },
                    templateUrl: templatePath,
                    parent: angular.element(document.body),
                    targetEvent: $event,
                    clickOutsideToClose: true
                });
        }
    }
}])


.factory('salaryDetails', ['$http', '$httpParamSerializerJQLike', function($http, $httpParamSerializerJQLike) {
    return {
        sendSalaryDetails: function(postData){
            return $http({
                url: '/workers/salary-details',
                method: 'POST',
                data: $httpParamSerializerJQLike(postData),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        }
    }
}])

.factory('getSalaryDetails', ['$http', '$httpParamSerializerJQLike', function($http, $httpParamSerializerJQLike) {
    return {
        sendData: function(worker_id, postData){
            return $http({
                url: "/workers/salary-details/"+ worker_id +"",
                method: 'POST',
                data: $httpParamSerializerJQLike(postData),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        }
    }
}])

.factory('sendValue', ['$http', '$httpParamSerializerJQLike', function($http, $httpParamSerializerJQLike) {
    return {
        sendYearValue: function(postData, path){
            return $http({
                url: path,
                method: 'POST',
                data: $httpParamSerializerJQLike(postData),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
      }
    }
}])


.factory('Excel',function($window){
        var uri='data:application/vnd.ms-excel;base64,',
            template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
            base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
            format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
        return {
            tableToExcel:function(tableId,worksheetName){
                var table=$(tableId),
                    ctx={worksheet:worksheetName,table:table.html()},
                    href=uri+base64(format(template,ctx));
                return href;
            }
        };
    })


.factory('afterSuccess',['$mdToast','$document', '$timeout', '$window', 
        function($mdToast, $document, $timeout, $window){
    return {
        showToast: function(message){
            var el = $document[0].querySelector('#toastDiv');
            $mdToast.show(
                $mdToast.simple()
                .textContent(message)
                .position('bottom left')
                .hideDelay(1200)
                .parent(el)
            );
        },
        redirect: function(url){
            function toPage(){
                $window.location.href = url;
            }
            $timeout(toPage, 2000); 
        }
    }
}])


.factory('onError',[function(){
    return {
        scrollUp: function(){
            $("html, body").animate({
                            scrollTop: 0
                        }, 600);
        }
    }
}]);
