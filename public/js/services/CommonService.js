angular.module('CommonService', [])

.factory('pagination', [function() {
    return {
        query: function(orderByCol, rowsPerPage, firstPage){
            return {
                order: orderByCol,
                limit: rowsPerPage,
                page: firstPage
            }
        },
        options: function(boolValue1, boolValue2, boolValue3){
            return {
                boundaryLinks: boolValue1,
                limitSelect: boolValue2,
                pageSelect: boolValue3
            }
        },
        limitOptions: function(itemsArray, rowsPerPage){
            return [rowsPerPage, rowsPerPage + rowsPerPage, {
                label: 'All',
                value: function () {
                    return itemsArray ? itemsArray.length : 0;
                }
            }];
        }
    }
}]);
