angular.module('SitesService', ['ngResource'])


.factory('getEditSiteData', ['$http', '$httpParamSerializerJQLike', function($http, $httpParamSerializerJQLike) {
    return {
        getSiteData: function(site_id){          
            return $http.get("/sites/edit/"+ site_id +"");
        }
      }
}])


.factory('getWorkersOnSite', ['$http', '$httpParamSerializerJQLike', function($http, $httpParamSerializerJQLike) {
    return {
        getWorkers: function(site_id){          
            return $http.get("/sites/"+ site_id +"");
        }
      }
}])


.factory('postEditSiteData', ['$http', '$httpParamSerializerJQLike', function($http, $httpParamSerializerJQLike) {
    return {
        postUserData: function(site_id, postData){
        return $http({
                url: "/sites/edit/"+ site_id +"",
                method: 'POST',
                data: $httpParamSerializerJQLike(postData),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        }
    }
}])


.factory('addNewSite', ['$http', '$httpParamSerializerJQLike', function($http, $httpParamSerializerJQLike) {
    return {
        postAddNewSite: function(postData){
        console.log("postData.close_date from service: " + postData.close_date);
            return $http({
                url: '/sites/add-new',
                method: 'POST',
                data: $httpParamSerializerJQLike(postData),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        }
    }
}])


.factory('sitesData', ['$resource', function($resource) {
  return $resource('/sites', {}, {
        query: { method:'GET', isArray: true }
               }); 
}])


.factory('siteDesignations', ['$resource', function($resource) {
  return $resource('/sites/posts', {}, {
        query: { method:'GET', isArray: true }
               }); 
}]);
