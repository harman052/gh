/* SETUP */

/* Calling modules into app */
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var router = express.Router();

/* CONFIGURATION */
	
/* Calling database */
var db = require('./config/db');

/* Setting up the body parser, module that extracts entire body
 * portion from the incoming requests */

/* Parse application/json */
app.use(bodyParser.json()); 

/* Parse application/vnd.api+json as json */
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); 

/* Parse application/x-www-form-urlencoded */
app.use(bodyParser.urlencoded({ extended: true })); 

/* Override with the X-HTTP-Method-Override header in the request. 
 * Simulate DELETE/PUT */
app.use(methodOverride());

/* Set the static files location /public/img will be /img for users */
app.use(express.static(__dirname + '/public')); 
/*
app.use('/js', express.static(__dirname + '/js'));
app.use('/controllers', express.static(__dirname + '/../controllers'));
app.use('/css', express.static(__dirname + '/css'));
app.use('/services', express.static(__dirname + '/services'));

app.all('/*', function(req, res, next) {
    // Just send the index.html for other files to support HTML5Mode
    res.sendFile('public/index.html', { root: __dirname });
});
*/
/* Calling routes. routes/index.js is main routing file 
 * that calls all other routes */
app.use('/', require('./routes/index'));


app.set('port', process.env.OPENSHIFT_NODEJS_PORT || 8080);
app.set('ip', process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1');

app.listen(app.get('port'), app.get('ip'), function(){
      console.log('Application is now running. Open "localhost:' + app.get('port')+'" in browser.');
});


/* Specifying port 
var port = process.env.PORT || 8001;
app.listen(port);	
console.log('App is live at ' + port);
//exports = module.exports = app;
*/