var express = require('express')
  , router = express.Router()

    router.use('/sites', require('./sites'))
    router.use('/workers', require('./workers'))

    router.get('/api', function(req, res) {
          res.send('Home page')
    })

router.get('/about', function(req, res) {
      res.send('Learn about us')
})

module.exports = router
