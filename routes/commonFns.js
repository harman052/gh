
/* The date received through POST request is converted into ISO 
 * format. This function reconverts date into IST and reformat it to 
 * store into database. 
 *
 * It takes date as an argument and returns formatted date.
 */

var maggi = function(date){
    var parse_start_date = new Date(date);
    var start_day = parse_start_date.getDate();
    var start_month = parse_start_date.getMonth() + 1;
    var start_year = parse_start_date.getFullYear();
    return correctDate = start_year +'-'+ start_month +'-'+ start_day;
}

