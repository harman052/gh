/* API functions for "Workers" page. */

var express = require('express');
var router = express.Router();
var path = require('../config/paths.js');
var db = require('../config/db');

function processDate(date){
    var parse_start_date = new Date(date);
    var start_day = parse_start_date.getDate();
    var start_month = parse_start_date.getMonth() + 1;
    var start_year = parse_start_date.getFullYear();
    return correctDate = start_year +'-'+ start_month +'-'+ start_day;
}

/* Adding new worker */
router.post(path.addNewWorker, function(req, res) {
    var first_name = req.body.first_name;
    var father_first_name = req.body.father_first_name;
    var sex = req.body.sex;
    var address = req.body.address;
    var phone = req.body.phone;
    var date_of_birth = processDate(req.body.date_of_birth);
    var post_id = req.body.post_id;
    var joining_date = processDate(req.body.joining_date);
            
    console.log("from backend --query: " + first_name);

    db.query(`INSERT INTO workers (workers.first_name, 
            workers.father_first_name, workers.sex, workers.address, 
            workers.phone, workers.date_of_birth, workers.post_id, 
            workers.joining_date) 
        VALUES ("`+ first_name +`","`+ father_first_name +`", 
                "`+ sex +`", "`+ address +`", "`+ phone +`", 
                "`+ date_of_birth +`", "`+ post_id +`", 
                "`+ joining_date +`")`, 
        function(err, rows){
        if(!err){
            res.send("1");
            console.log("from backend --success: " + rows);
        } else {
            res.send(err);
            console.log("from backend --error: " + err);
        }
    });
});

/* Fetching all workers */
router.get(path.workersPage, function(req, res) {
    db.query(`SELECT workers.worker_id, workers.first_name, 
        workers.father_first_name, workers.sex, workers.address, 
        workers.phone, workers.date_of_birth, workers.post_id,
        workers.joining_date,  salary.site_id, salary.rate, 
        sites.site_name, posts.post FROM workers LEFT JOIN salary ON 
        workers.worker_id = salary.worker_id AND date IN 
        (SELECT MAX(date) FROM salary 
        WHERE salary.worker_id = workers.worker_id) LEFT JOIN sites ON 
        salary.site_id = sites.site_id LEFT JOIN posts ON workers.post_id = posts.post_id`, function(err, rows){
        if(!err){
            res.send(rows);
        } else {
            res.send(err);
            console.log(err);
        }
    });
});


/* Fetching worker with id */
router.get('/edit/:worker_id', function(req, res) {
    db.query(`SELECT workers.* FROM workers 
        WHERE workers.worker_id = "`+ req.params.worker_id +`";`, function(err, rows){
        if(!err){
            res.send(rows);
        } else {
            res.send(err);
            console.log(err);
        }
    });
});


/* Edit by sending id in URL. */
router.post('/edit/:worker_id', function(req, res) {
    var worker_id = req.params.worker_id // getting id from URL.
    var first_name = req.body.first_name; // getting variables from request body
    var father_first_name = req.body.father_first_name;
    var sex = req.body.sex;
    var address = req.body.address;
    var phone = req.body.phone;
    var date_of_birth = processDate(req.body.date_of_birth);
    var post_id = req.body.post_id;
    var joining_date = processDate(req.body.joining_date);
 
    db.query(`UPDATE workers SET workers.first_name = "`+ first_name +`",
        workers.father_first_name = "`+ father_first_name +`",
        workers.sex = "`+ sex +`", workers.address = "`+ address +`",
        workers.phone = "`+ phone +`", 
        workers.date_of_birth = "`+ date_of_birth +`",
        workers.post_id = "`+ post_id +`",
        workers.joining_date = "`+ joining_date +`"
        WHERE worker_id = "`+ worker_id +`"`, function(err, rows){
            if(!err){
                res.send(rows);
            } else {
                res.send(err);
                console.log(err);
            }
        });
});

/* Fetching list of worker posts */
router.get('/posts', function(req, res) {
    db.query(`SELECT * from posts`, function(err, rows){
        if(!err){
            res.send(rows);
        } else {
            res.send(err);
            console.log(err);
        }
    });
});


/* Adding new worker post */
router.post('/posts/add-new', function(req, res) {
    var post = req.body.post;
    db.query(`INSERT into posts (post) VALUES ("`+ post +`")`, function(err, rows){
        if(!err){
            res.send(rows);
        } else {
            res.send(err);
            console.log(err);
        }
    });
});


/* Updating worker post */
router.post('/posts/edit', function(req, res) {
    var post = req.body.post;
    var post_id = req.body.post_id;
    db.query(`UPDATE posts SET post = "`+ post +`" WHERE  post_id = "`+ post_id +`"`, function(err, rows){
        if(!err){
            res.send(rows);
        } else {
            res.send(err);
            console.log(err);
        }
    });
});


/* Deleting worker post */
router.post('/posts/delete', function(req, res) {
    var post_ids = req.body.postIds;
    db.query(`DELETE from posts WHERE post_id IN (`+ post_ids +`)`, function(err, rows){
        if(!err){
            res.send(rows);
        } else {
            res.send(err);
            console.log(err);
        }
    });
});


/* Fetching list of site status 
 * THIS ROUTE SHOULD BE IN sites.js FILE, BUT IS NOT WORKING THERE. 
 */
router.get('/site-status', function(req, res) {
    db.query('SELECT * FROM site_status', function(err, rows){
        if(!err){
            res.send(rows);
            console.log("success: "+rows);
        } else {
            res.send(err);
            console.log("error: "+err);
        }
    });
});


/* Adding new site status 
 * THIS ROUTE SHOULD BE IN sites.js FILE, BUT IS NOT WORKING THERE. 
 */
router.post('/site-status/add-new', function(req, res) {
    var status = req.body.status;
    db.query(`INSERT into site_status (status) VALUES ("`+ status +`")`, function(err, rows){
        if(!err){
            res.send(rows);
        } else {
            res.send(err);
            console.log(err);
        }
    });
});


/* Updating site status 
 * THIS ROUTE SHOULD BE IN sites.js FILE, BUT IS NOT WORKING THERE. 
 */
router.post('/site-status/edit', function(req, res) {
    var status = req.body.status;
    var status_id = req.body.status_id;
    db.query(`UPDATE site_status SET status = "`+ status +`" WHERE  status_id = "`+ status_id +`"`, function(err, rows){
        if(!err){
            res.send(rows);
        } else {
            res.send(err);
            console.log(err);
        }
    });
});


/* Deleting worker post 
 * THIS ROUTE SHOULD BE IN sites.js FILE, BUT IS NOT WORKING THERE. 
 */
router.post('/site-status/delete', function(req, res) {
    var status_ids = req.body.statusIds;
    db.query(`DELETE from site_status WHERE status_id IN (`+ status_ids +`)`, function(err, rows){
        if(!err){
            res.send(rows);
        } else {
            res.send(err);
            console.log(err);
        }
    });
});


/* Deleting workers from table */
router.post('/delete', function(req, res) {
    var worker_ids = req.body.workerIds
    console.log(worker_ids);
    db.query(`DELETE FROM workers WHERE worker_id IN (`+ worker_ids +`)`, function(err, rows){
        if(!err){
            res.send(rows);
        } else {
            res.send(err);
            console.log(err);
        }
    });
});


/* Moving workers to site */
router.post('/move-to-site', function(req, res) {
    var worker_ids = req.body.workerIds
    var site_id = req.body.siteId
    console.log(worker_ids);
    console.log(site_id);
    
    db.query(`SELECT EXISTS(SELECT 1 FROM salary WHERE 
        salary.worker_id = "`+ worker_ids +`") AS result`, 
        function(err, rows) {
            if(!err){
                console.log("result of search query: "+ rows[0].result);
                if (rows[0].result == 0){
                    db.query(`INSERT INTO salary (salary.worker_id, 
                        salary.site_id, salary.date) VALUES 
                        ("`+ worker_ids +`", "`+ site_id +`", NOW())`, 
                        function(err, rows){
                            if(!err){
                                res.send(rows);
                                console.log("Worker successfully added to given site"+ JSON.stringify(rows));
                            } else {
                                res.send(err);
                                console.log(err);
                                console.log("An error occurred: "+ JSON.stringify(err));
                            }
                        });
                } else {
                    console.log("Moving worker to new site...");
                //    res.send("value exists!. "+rows);

                    db.query(`INSERT INTO salary (worker_id, rate, 
                        date, site_id) SELECT worker_id, rate, 
                        NOW(), "`+ site_id +`" FROM salary WHERE 
                        date = (SELECT MAX(date) FROM salary where 
                        worker_id= "`+ worker_ids +`") AND 
                        worker_id = "`+ worker_ids +`"`, 
                        function(err, rows){
                            if(!err){
                                res.send(rows);
                                console.log("Worker successfully moved to given site: "+ JSON.stringify(rows));
                            } else {
                                res.send(err);
                                console.log(err);
                                console.log("Error occurred: "+ JSON.stringify(err));
                            }
                        });
                }
            } else {
                    res.send(err);
                    console.log("An error occured while looking for rows:"+ err);
                }
            });
});


/* Set rates of workers */
router.post('/set-rate', function(req, res) {
    var worker_ids = req.body.workerIds
    var rate = req.body.siteId
    console.log(worker_ids);
    console.log("this is worker's new rate: "+rate);
    
    db.query(`SELECT EXISTS(SELECT 1 FROM salary WHERE 
        salary.worker_id = "`+ worker_ids +`") AS result`, 
        function(err, rows) {
            if(!err){
                console.log("result of exists query: "+ rows[0].result);
                if (rows[0].result == 0){
                    console.log("First add worker to site!");
                    res.send("0");
                } else {
                    console.log("Setting worker rate...");
                //    res.send("value exists!. "+rows);

                    db.query(`UPDATE salary SET rate =  "`+ rate +`", 
                        amount = "`+ rate +`" * no_of_days, balance = amount - advance 
                        WHERE YEAR(salary.date) = DATE_FORMAT(CURDATE(), '%Y') 
                        AND MONTH(salary.date) = DATE_FORMAT(CURDATE(), '%m') 
                        AND worker_id = "`+ worker_ids +`"`, 
                        function(err, rows){
                            if(!err){
                                res.send("1");
                                console.log("Rate is successfully set"+ JSON.stringify(rows));
                            } else {
                                res.send(err);
                                console.log(err);
                                console.log("Error occurred"+ JSON.stringify(err));
                            }
                        });
                }
            } else {
                    res.send(err);
                    console.log("An error occured while looking for rows:"+ JSON.stringify(err));
                }
            });
});


/* Saving salary details of worker */
router.post('/salary-details', function(req, res) {
    var no_of_days = req.body.no_of_days;
    var advance = req.body.advance;
    var  worker_id = req.body.worker_id;
    var sal_id = req.body.sal_id;
    
    console.log("days, adv: " + no_of_days + ", " + advance);

    db.query(`UPDATE salary SET no_of_days = "`+ no_of_days +`", 
        amount = rate * "`+ no_of_days +`", advance =  "`+ advance +`", 
        balance = amount-advance WHERE sal_id = "`+ sal_id +`"`, 
        function(err, rows){
        if(!err){
            res.send(rows);
        } else {
            res.send(err);
            console.log(err);
        }
    });
});


/* Sending salary details of specific worker */
router.post('/salary-details/:worker_id', function(req, res) {
    var sal_id = req.body.sal_id;
       db.query(`SELECT no_of_days, advance FROM salary 
           WHERE sal_id = "`+ sal_id +`"`, 
        function(err, rows){
        if(!err){
            res.send(rows);
        } else {
            res.send(err);
            console.log(err);
        }
    });
});


/* Getting year value to fetch rates of workers from that year */
router.post('/rates', function(req, res) {
    var selectedYear = req.body.year;
    db.query(`SELECT workerName, year, post,
            max(Jan) as Jan,
            max(Feb) as Feb,
            max(Mar) as Mar,
            max(Apr) as Apr,
            max(May) as May,
            max(Jun) as Jun,
            max(Jul) as Jul,
            max(Aug) as Aug,
            max(Sep) as Sep,
            max(Oct) as Oct,
            max(Nov) as Nov,
            max(Decm) as Decm
        from (SELECT workers.first_name AS workerName, 
        workers.worker_id AS workerId, year(date) AS year, posts.post AS post,
        case when month(date)= 1 and year(date) = "`+ selectedYear +`" then salary.rate end as Jan,
        case when month(date)= 2 and year(date) = "`+ selectedYear +`" then salary.rate end as Feb,
        case when month(date)= 3 and year(date) = "`+ selectedYear +`" then salary.rate end as Mar,
        case when month(date)= 4 and year(date) = "`+ selectedYear +`" then salary.rate end as Apr,
        case when month(date)= 5 and year(date) = "`+ selectedYear +`" then salary.rate end as May,
        case when month(date)= 6 and year(date) = "`+ selectedYear +`" then salary.rate end as Jun,
        case when month(date)= 7 and year(date) = "`+ selectedYear +`" then salary.rate end as Jul,
        case when month(date)= 8 and year(date) = "`+ selectedYear +`" then salary.rate end as Aug,
        case when month(date)= 9 and year(date) = "`+ selectedYear +`" then salary.rate end as Sep,
        case when month(date)= 10 and year(date) = "`+ selectedYear +`" then salary.rate end as Oct,
        case when month(date)= 11 and year(date) = "`+ selectedYear +`" then salary.rate end as Nov,
        case when month(date)= 12 and year(date) = "`+ selectedYear +`" then salary.rate end as Decm
        from workers LEFT JOIN salary ON workers.worker_id = salary.worker_id LEFT JOIN posts ON 
        workers.post_id = posts.post_id) 
        as months group by workerId; `, function(err, rows){
        if(!err){
            res.send(rows);
        } else {
            res.send(err);
            console.log(err);
        }
        });
});

module.exports = router
