/* API functions for "Sites" page. */

var express = require('express');
var router = express.Router();
var path = require('../config/paths.js');
var db = require('../config/db');

function processDate(date){
    var parse_start_date = new Date(date);
    var start_day = parse_start_date.getDate();
    var start_month = parse_start_date.getMonth() + 1;
    var start_year = parse_start_date.getFullYear();
    return correctDate = start_year +'-'+ start_month +'-'+ start_day;
}

/* Adding new site */
router.post(path.addNewSite, function(req, res) {
    var site_name = req.body.site_name;
    var site_location = req.body.site_location;
    var start_date = processDate(req.body.start_date);
    var close_date = processDate(req.body.close_date);
    var status_id = req.body.status_id;


    db.query(`INSERT INTO sites (site_name, site_location, start_date, close_date, status_id) 
        VALUES ("`+ site_name +`","`+ site_location +`","`+ start_date +`","`+ close_date +`","`+ status_id +`")`, 
        function(err, rows){
        if(!err){
            res.send(rows);
        } else {
            res.send(err);
            console.log(err);
        }
    });
})


/* Fetching all sites */
router.get(path.sitesPage, function(req, res) {
    db.query('SELECT sites.*, site_status.status FROM sites LEFT JOIN site_status ON sites.status_id = site_status.status_id', function(err, rows){
        if(!err){
            res.send(rows);
        } else {
            res.send(err);
            console.log(err);
        }
    });
});


/* Edit by sending id in URL. */
router.post('/edit/:site_id', function(req, res) {
    var site_id = req.params.site_id // getting id from URL.
    var site_name = req.body.site_name; // getting variables from request body
    var site_location = req.body.site_location;
    var start_date = processDate(req.body.start_date);
    var close_date = processDate(req.body.close_date);
    var status_id = req.body.status_id;

    db.query(`UPDATE sites SET sites.site_name = "`+ site_name +`",
        sites.site_location = "`+ site_location +`",
        sites.start_date = "`+ start_date +`",
        sites.close_date = "`+ close_date +`",
        sites.status_id = "`+ status_id +`"
        WHERE site_id = "`+ site_id +`"`, function(err, rows){
            if(!err){
                res.send(rows);
            } else {
                res.send(err);
                console.log(err);
            }
        });
});


/* Fetching site with id */
router.get('/edit/:site_id', function(req, res) {
    db.query(`SELECT sites.*, site_status.status FROM sites LEFT JOIN
        site_status ON sites.status_id = site_status.status_id
        WHERE sites.site_id = "`+ req.params.site_id +`";`, function(err, rows){
        if(!err){
            res.send(rows);
        } else {
            res.send(err);
            console.log(err);
        }
    });
});


/* Fetching all workers working at given site using site_id for 
 * latest month. This is default view of all sites. 
 */
router.get('/:site_id', function(req, res) {
    db.query(`SELECT workers.worker_id, workers.first_name,
            workers.post_id, salary.sal_id, salary.rate, 
            salary.no_of_days, salary.amount, salary.advance, 
            salary.balance, MONTH(salary.date) as month, 
            YEAR(salary.date) as year, posts.post FROM workers LEFT 
            JOIN posts ON workers.post_id = posts.post_id
            INNER JOIN salary on salary.worker_id = workers.worker_id 
            WHERE salary.site_id  = "`+ req.params.site_id +`" AND 
            MONTH(date) IN (SELECT MONTH(MAX(date)) FROM salary WHERE  
            salary.site_id  = "`+ req.params.site_id +`") AND
            YEAR(date) IN (SELECT YEAR(MAX(date)) FROM salary WHERE  
            salary.site_id  = "`+ req.params.site_id +`")`, 
        function(err, rows){
            if(!err){
            res.send(rows);
        } else {
            res.send(err);
            console.log(err);
        }
    });
});


/* Fetching all workers worked at given site using site_id for given
 * month and year.
 */
router.post('/oldData', function(req, res) {
    var year = req.body.year;
    var month = req.body.month;
    var site_id = req.body.site_id;

    db.query(`SELECT workers.worker_id, workers.first_name, 
            workers.post_code, salary.rate, salary.no_of_days, 
            salary.amount, salary.advance, salary.balance FROM 
            workers INNER JOIN salary on 
            salary.worker_id = workers.worker_id WHERE 
            salary.site_id  = "`+ site_id +`" AND 
            YEAR(salary.date) =  "`+ year +`"AND 
            MONTH(salary.date) = "`+ month +`"`, 
        function(err, rows){
        if(!err){
            res.send(rows);
        } else {
            res.send(err);
            console.log(err);
        }
    });
});


/* Deleting records from table */
router.post('/delete', function(req, res) {
    var site_ids = req.body.siteIds
    console.log(site_ids);
    db.query(`DELETE FROM sites WHERE site_id IN (`+ site_ids +`)`, function(err, rows){
        if(!err){
            res.send(rows);
        } else {
            res.send(err);
            console.log(err);
        }
    });
});


/* Deleting workers from site */
router.post('/delete-workers', function(req, res) {
    var sal_ids = req.body.salIds
    console.log(sal_ids);
    db.query(`DELETE FROM salary WHERE sal_id IN (`+ sal_ids +`)`, function(err, rows){
        if(!err){
            res.send(rows);
        } else {
            res.send(err);
            console.log(err);
        }
    });
});


/* Create new month sheet. */
router.post('/new-month-sheet/:site_id', function(req, res) {

    var site_id = req.body.siteId // getting id from URL.
    var worker_ids = req.body.workerIds;

    console.log("values: "+site_id+" "+worker_ids);

    db.query(`SELECT worker_id, site_id, rate FROM salary WHERE 
        date IN (SELECT MAX(date) FROM salary GROUP BY worker_id) 
        AND salary.site_id = "`+ site_id +`"`, 
        function(err, rows){
            if(!err){
                res.send(rows);
            } else {
                res.send(err);
                console.log(err);
            }
        });
});

/* Create new month sheet. */
router.post('/new-month/add-workers', function(req, res) {

    var site_id = req.body.siteId // getting id from URL.
    var worker_ids = req.body.workerIds;

    console.log("values: "+site_id+" "+worker_ids);

    db.query(`INSERT INTO salary (worker_id, rate, date, site_id) 
            SELECT worker_id, rate, NOW(), site_id FROM salary 
            WHERE date IN (SELECT MAX(date) FROM salary GROUP BY 
                worker_id) and worker_id IN (`+ worker_ids +`); `, 
        function(err, rows){
            if(!err){
                res.send(rows);
            } else {
                res.send(err);
                console.log(err);
            }
        });
});

/* Getting year value to fetch reports of all sites from for year */
router.post('/reports', function(req, res) {
    var selectedYear = req.body.year;
     db.query(`SELECT siteName, siteId, year, status,
            sum(Jan) as Jan,
            sum(Feb) as Feb,
            sum(Mar) as Mar,
            sum(Apr) as Apr,
            sum(May) as May,
            sum(Jun) as Jun,
            sum(Jul) as Jul,
            sum(Aug) as Aug,
            sum(Sep) as Sep,
            sum(Oct) as Oct,
            sum(Nov) as Nov,
            sum(Decm) as Decm
        from (SELECT sites.site_name AS siteName,
        sites.site_id AS siteId, year(date) AS year, site_status.status AS status,
        case when month(date)= 1 and year(date) = "`+ selectedYear +`" then salary.amount end as Jan,
        case when month(date)= 2 and year(date) = "`+ selectedYear +`" then salary.amount end as Feb,
        case when month(date)= 3 and year(date) = "`+ selectedYear +`" then salary.amount end as Mar,
        case when month(date)= 4 and year(date) = "`+ selectedYear +`" then salary.amount end as Apr,
        case when month(date)= 5 and year(date) = "`+ selectedYear +`" then salary.amount end as May,
        case when month(date)= 6 and year(date) = "`+ selectedYear +`" then salary.amount end as Jun,
        case when month(date)= 7 and year(date) = "`+ selectedYear +`" then salary.amount end as Jul,
        case when month(date)= 8 and year(date) = "`+ selectedYear +`" then salary.amount end as Aug,
        case when month(date)= 9 and year(date) = "`+ selectedYear +`" then salary.amount end as Sep,
        case when month(date)= 10 and year(date) = "`+ selectedYear +`" then salary.amount end as Oct,
        case when month(date)= 11 and year(date) = "`+ selectedYear +`" then salary.amount end as Nov,
        case when month(date)= 12 and year(date) = "`+ selectedYear +`" then salary.amount end as Decm
        FROM sites LEFT JOIN salary ON sites.site_id = salary.site_id  LEFT JOIN site_status ON 
        sites.status_id = site_status.status_id)
        AS months GROUP BY siteId`, function(err, rows){
        if(!err){
            res.send(rows);
        } else {
            res.send(err);
            console.log(err);
        }
        });
});


module.exports = router
