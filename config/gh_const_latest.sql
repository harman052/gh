-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 07, 2016 at 11:48 AM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gh_const`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `months`
--
CREATE TABLE IF NOT EXISTS `months` (
`sal_id` int(11)
,`worker_id` int(11)
,`site_id` int(11)
,`rate` decimal(15,3)
,`no_of_days` decimal(15,3)
,`amount` decimal(15,3)
,`advance` decimal(15,3)
,`balance` decimal(15,3)
,`date` datetime
,`Jan` decimal(15,3)
,`Feb` decimal(15,3)
,`Mar` decimal(15,3)
,`Apr` decimal(15,3)
,`May` decimal(15,3)
);
-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `post` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `salary`
--

CREATE TABLE IF NOT EXISTS `salary` (
  `sal_id` int(11) NOT NULL AUTO_INCREMENT,
  `worker_id` int(11) DEFAULT NULL,
  `site_id` int(11) DEFAULT NULL,
  `rate` decimal(15,3) DEFAULT '0.000',
  `no_of_days` decimal(15,3) DEFAULT '0.000',
  `amount` decimal(15,3) DEFAULT '0.000',
  `advance` decimal(15,3) DEFAULT '0.000',
  `balance` decimal(15,3) DEFAULT '0.000',
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`sal_id`),
  UNIQUE KEY `sal_id` (`sal_id`),
  KEY `worker_id` (`worker_id`),
  KEY `site_id_fk` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=348 ;

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE IF NOT EXISTS `sites` (
  `site_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_name` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `site_location` text CHARACTER SET utf8,
  `start_date` date DEFAULT NULL,
  `close_date` date DEFAULT NULL,
  `status_id` int(1) DEFAULT NULL,
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=69 ;

-- --------------------------------------------------------

--
-- Table structure for table `site_status`
--

CREATE TABLE IF NOT EXISTS `site_status` (
  `status_id` int(1) NOT NULL AUTO_INCREMENT,
  `status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `workers`
--

CREATE TABLE IF NOT EXISTS `workers` (
  `worker_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `father_first_name` varchar(30) CHARACTER SET utf32 DEFAULT NULL,
  `sex` char(1) CHARACTER SET utf8 DEFAULT NULL,
  `address` text CHARACTER SET utf8,
  `phone` bigint(1) DEFAULT NULL,
  `date_of_birth` date DEFAULT '0000-00-00',
  `post_id` int(1) DEFAULT NULL,
  `joining_date` date DEFAULT '0000-00-00',
  PRIMARY KEY (`worker_id`),
  KEY `worker_id` (`worker_id`),
  KEY `post` (`post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=182 ;

-- --------------------------------------------------------

--
-- Structure for view `months`
--
DROP TABLE IF EXISTS `months`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `months` AS (select `salary`.`sal_id` AS `sal_id`,`salary`.`worker_id` AS `worker_id`,`salary`.`site_id` AS `site_id`,`salary`.`rate` AS `rate`,`salary`.`no_of_days` AS `no_of_days`,`salary`.`amount` AS `amount`,`salary`.`advance` AS `advance`,`salary`.`balance` AS `balance`,`salary`.`date` AS `date`,(case when (month(`salary`.`date`) = 1) then `salary`.`rate` end) AS `Jan`,(case when (month(`salary`.`date`) = 2) then `salary`.`rate` end) AS `Feb`,(case when (month(`salary`.`date`) = 3) then `salary`.`rate` end) AS `Mar`,(case when (month(`salary`.`date`) = 4) then `salary`.`rate` end) AS `Apr`,(case when (month(`salary`.`date`) = 5) then `salary`.`rate` end) AS `May` from `salary`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `salary`
--
ALTER TABLE `salary`
  ADD CONSTRAINT `salary_ibfk_1` FOREIGN KEY (`worker_id`) REFERENCES `workers` (`worker_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `salary_ibfk_2` FOREIGN KEY (`site_id`) REFERENCES `sites` (`site_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
