-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 24, 2016 at 09:37 AM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gh_const`
--

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `activity` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `post` varchar(10) DEFAULT NULL,
  `post_code` char(1) DEFAULT NULL,
  PRIMARY KEY (`post_id`),
  KEY `post_code` (`post_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`post_id`, `post`, `post_code`) VALUES
(1, 'Mistri', 'M'),
(2, 'Labour', 'L'),
(3, 'Engineer', 'E');

-- --------------------------------------------------------

--
-- Table structure for table `salary`
--

CREATE TABLE IF NOT EXISTS `salary` (
  `sal_id` int(11) NOT NULL AUTO_INCREMENT,
  `worker_id` int(11) DEFAULT NULL,
  `no_of_days` decimal(15,2) DEFAULT NULL,
  `amount` decimal(15,2) DEFAULT NULL,
  `advance` decimal(15,2) DEFAULT NULL,
  `balance` decimal(15,2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`sal_id`),
  KEY `worker_id` (`worker_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `salary`
--

INSERT INTO `salary` (`sal_id`, `worker_id`, `no_of_days`, `amount`, `advance`, `balance`, `date`) VALUES
(1, 50, 10.00, 3000.00, 1000.00, 2000.00, '2016-03-03'),
(2, 51, 25.00, 5000.00, 1000.00, 4000.00, '2015-12-13'),
(3, 96, 14.00, 1400.00, 0.00, 1400.00, '2016-04-06'),
(4, 109, 17.00, 850000.00, 0.00, 850000.00, '2016-04-08'),
(6, 110, 26.87, 13435.00, 450.00, 12985.00, '2016-04-06'),
(7, 111, 30.50, 20435.00, 0.00, 20435.00, NULL),
(8, 112, 26.77, 14989.35, 3234.00, 11755.35, '2016-05-22'),
(9, 113, 22.00, 7590.00, 7623.67, -33.67, NULL),
(10, 114, 29.00, 17400.00, 0.00, 17400.00, NULL),
(11, 115, NULL, NULL, NULL, NULL, NULL),
(12, 116, 31.00, 17515.00, 3455.00, 14060.00, '2016-04-22'),
(13, 117, NULL, NULL, NULL, NULL, '2016-05-22'),
(14, 118, NULL, NULL, NULL, NULL, '2016-05-22'),
(15, 119, NULL, NULL, NULL, NULL, '2016-05-23'),
(16, 112, NULL, NULL, NULL, NULL, '2016-06-22'),
(17, 117, NULL, NULL, NULL, NULL, '2016-06-22'),
(18, 118, NULL, NULL, NULL, NULL, '2016-06-22');

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE IF NOT EXISTS `sites` (
  `site_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_name` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `site_location` text CHARACTER SET utf8,
  `start_date` date DEFAULT NULL,
  `close_date` date DEFAULT NULL,
  `status` char(1) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=58 ;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`site_id`, `site_name`, `site_location`, `start_date`, `close_date`, `status`) VALUES
(0, 'No site', NULL, NULL, NULL, NULL),
(17, 'Sterling Resorts', 'Pakhowal Road, Ludhiana', '2016-05-16', '2016-02-14', 'C'),
(18, 'Honda Showroom', 'Ludhiana, Punjab', '2015-12-05', '2016-04-02', 'O'),
(19, 'Haveli', 'Ldh', '2010-03-14', '2015-11-28', 'C'),
(20, 'Red Mango', 'Doraha', '2000-09-11', '2016-01-13', 'C'),
(37, 'Akash Building', 'Near Jagraon Bridge', '2016-05-01', '2016-05-14', 'C'),
(38, 'RX Exports', 'Dhandhari Kalan, Ludhiana', '2016-05-03', '2016-05-14', 'C'),
(42, 'Apollo Hospital', 'Sherpur Chowk, Ludhiana', '2010-07-12', '2016-04-15', 'C'),
(43, 'Maharaja Grand', 'Mullanpur, Ludhiana', '1991-01-07', '2016-00-01', 'O'),
(45, 'GNE College', 'Ludhiana', '2016-04-04', '2016-04-14', 'C'),
(46, 'Bonn Breads', 'Focal Point, Ludhiana, Punjab', '2016-03-01', '2016-04-01', 'C'),
(51, 'Damint', 'Park Ave', '2013-10-06', '2016-05-19', 'O'),
(53, 'NSPS School', 'Gill Road', '2016-05-15', '0000-00-00', 'O'),
(55, 'GK Resorts', 'Pakhowal Road, Ludhiana', '2016-01-07', '0000-00-00', 'C'),
(56, 'Guru Gobind Singh To', 'Dholewal Chowk', '1993-01-08', '1995-12-07', 'C'),
(57, 'CMC Hospital', 'Ludhiana, Punjab', '1991-02-07', '2016-05-21', 'C');

-- --------------------------------------------------------

--
-- Table structure for table `workers`
--

CREATE TABLE IF NOT EXISTS `workers` (
  `worker_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `father_first_name` varchar(20) CHARACTER SET utf32 DEFAULT NULL,
  `father_last_name` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `mother_first_name` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `mother_last_name` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `sex` char(1) CHARACTER SET utf8 DEFAULT NULL,
  `address` text CHARACTER SET utf8,
  `phone` bigint(1) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `post_code` char(1) DEFAULT NULL,
  `joining_date` date DEFAULT NULL,
  `rate` decimal(10,0) DEFAULT NULL,
  `site_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`worker_id`),
  KEY `site` (`site_id`),
  KEY `site_2` (`site_id`),
  KEY `worker_id` (`worker_id`),
  KEY `post` (`post_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=120 ;

--
-- Dumping data for table `workers`
--

INSERT INTO `workers` (`worker_id`, `first_name`, `last_name`, `father_first_name`, `father_last_name`, `mother_first_name`, `mother_last_name`, `sex`, `address`, `phone`, `date_of_birth`, `post_code`, `joining_date`, `rate`, `site_id`) VALUES
(51, 'Shiva', 'Malhorta', 'Ram', 'Malhotra', 'Geeta', 'Malhotra', 'M', 'Bachittar Nagar', 99887766, '1991-02-07', 'M', '2016-05-20', 200, 19),
(96, 'Birju Lal', 'Sharma', 'Lal Chand', 'Sharma', 'Mohini', 'Sharma', 'M', '5065 St Lawrence Ave, San Antonio, CA', 99887764, '1991-02-05', 'M', '2009-02-01', 100, 18),
(105, 'Atma ram', 'Bhidey', 'TukaRam', 'Bhidey', 'Uma', 'Bhidey', 'M', 'Gokuldham Society, Mumbai, India', 998876, '1989-06-08', 'M', '2016-05-09', 600, 18),
(108, 'Sonia', 'Mani', 'Chet Ram', 'Mani', 'Gogna Devi', 'Mani', 'F', 'Kapurthalla', 2998887, '2007-07-21', 'L', '2005-07-09', 456, 18),
(109, 'Manju', 'Verma', 'Manoj', 'Verma', 'Kamla', 'Verma', 'F', 'Near Water tank Ludhiana', 9887766, '1991-02-07', 'E', '2000-01-01', 50000, 17),
(110, 'Sohit', 'Kumar', 'Rakesh', 'Kumar', 'Kamini', 'Kumari', 'M', 'Isher Nagar, Ludhiana', 9988771252, '1980-01-31', 'E', '2010-07-08', 500, 17),
(111, 'Jai', 'Chand', 'Rooplal', 'Chand', 'Rupali', 'Chanda', 'M', 'LUdhiana', 9988771252, '2015-05-04', 'M', '2016-05-02', 670, 20),
(112, 'Nitin', 'Arora', 'Navneet', 'Arora', 'Navjot', 'Arora', 'M', 'Ludhiana', 9988776655, '1984-06-06', 'L', '1984-10-31', 560, 18),
(113, 'Kiran', 'Goel', 'Jaspal', 'Goel', 'Mahinder', 'Goel', 'F', 'Ludhiana', 98765432324, '2011-07-25', 'E', '2015-05-30', 345, 18),
(114, 'Gurnaam', 'Singh', 'Manjinder', 'Singh', 'Gunwant', 'Kaur', 'M', 'Ludhiana', 8765432456, '1991-02-06', 'M', '2011-07-25', 600, 20),
(115, 'Sheila Devi', 'Ramchand', 'Bakshish', 'Singh', 'Jasowa', 'Singh', 'F', 'Ldh', 876543546, '2015-01-31', 'L', '2014-06-28', 100, 19),
(116, 'Murli Monahar', 'Joshi', 'Manohar', 'Joshi', 'Meena', 'Joshi', 'M', 'Ldh', 98765432344, '1990-06-20', 'L', '2016-05-22', 565, 18),
(117, 'Sunita', 'Yadav', 'Kailash', 'Yadav', 'Mohini', 'Yadav', 'F', 'Gill Village', 9876543234, '2006-07-24', 'L', '2014-10-21', 212, 17),
(118, 'Kailash', 'Khurana', 'Mahesh', 'Khurana', 'Anita', 'Khurana', 'M', 'Gill Village', 23456743, '2007-04-16', 'M', '2013-03-14', 341, 18),
(119, 'Mahesh', 'KUmar', 'Fateh', 'Kumar', 'irani', 'KUmari', 'M', 'Ldh', 98765435445, '1971-12-31', 'L', '1996-11-03', 334, 18);

--
-- Triggers `workers`
--
DROP TRIGGER IF EXISTS `new_worker_added`;
DELIMITER //
CREATE TRIGGER `new_worker_added` AFTER INSERT ON `workers`
 FOR EACH ROW BEGIN
INSERT INTO salary (worker_id, date)
Values (NEW.worker_id, CURDATE());
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `worker_location_history`
--

CREATE TABLE IF NOT EXISTS `worker_location_history` (
  `history_id` int(11) NOT NULL AUTO_INCREMENT,
  `worker_id` int(11) DEFAULT NULL,
  `current_site_id` int(11) DEFAULT NULL,
  `New_site_id` int(11) DEFAULT NULL,
  `transfer_date` date DEFAULT NULL,
  PRIMARY KEY (`history_id`),
  KEY `worker_id` (`worker_id`),
  KEY `current_site_id` (`current_site_id`),
  KEY `New_site_id` (`New_site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `workers`
--
ALTER TABLE `workers`
  ADD CONSTRAINT `workers_ibfk_2` FOREIGN KEY (`post_code`) REFERENCES `posts` (`post_code`);

--
-- Constraints for table `worker_location_history`
--
ALTER TABLE `worker_location_history`
  ADD CONSTRAINT `worker_location_history_ibfk_1` FOREIGN KEY (`worker_id`) REFERENCES `workers` (`worker_id`),
  ADD CONSTRAINT `worker_location_history_ibfk_2` FOREIGN KEY (`current_site_id`) REFERENCES `sites` (`site_id`),
  ADD CONSTRAINT `worker_location_history_ibfk_3` FOREIGN KEY (`New_site_id`) REFERENCES `sites` (`site_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
